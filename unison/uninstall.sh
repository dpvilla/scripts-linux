#/bin/bash

# Script de ejecución verión anterior
rm -f /root/bin/run_unison.pl

#- unison-logout.pl: el script que se llama para lanzar unison, versión anterior
rm -f /home/bin/unison-logout.pl

#- enlaces simbólicos para ejecución en logout
for i in david belen clara irene 
do
    # El viejo
    rm -f /home/$i/.config/plasma-workspace/shutdown/unison-logout.pl
    # El nuevo
    rm -f /home/$i/.config/plasma-workspace/shutdown/run_unison.pl
done


#- Desactivar servicio y timer
systemctl stop unison.timer


#- Borrar ficheros servicio y timer
rm -f /usr/lib/systemd/system/unison.*
systemctl daemon-reload

# Quitar unison de sudoers
rm -f /etc/sudoers.d/unison

