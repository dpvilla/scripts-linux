 
# Configuración de unison en casa
Tanto el portátil como el sobremesa hacen sincronización contra la raspberry pi.

## Descripción de funcionamiento
El sistema está preparado para ejecutarlo mediante un timer de systemd, se ejecuta 1 minuto después del arranque, cada 30 minutos, y al hacer logout en el sistema para provocar el copiado final de los datos.

Se sincronizan los siguientes elementos:
- El directorio de documentos compartidos (commons_docs)
- Los directorios "Escritorio" y "Documentos" de todos los usuarios.


## Instalación
Basta con ejecutar como root: `bash install.sh`


El script install.sh realiza los siguientes cambios:
- Crea el directorio /home/bin/ si no existe
- Copia en él el fihero run_unison.pl
- Crea el fichero de configuración
- Instala el servicio y el timer
- Activa los servicios
- Crea el enlace simbólico con el comando a ejecutar al salir


## Desinstalación
Ejecutar `bash uninstall.sh`.

El script desactiva el servicio, los timers y borra el enlace simbólico que se coloca en la ruta de scripts que se ejecutan en el logout de kde.


## Ficheros
La carpeta contiene los siguientes elementos:
- run_unison.pl: script a llamar mediante el cron o el servicio que provoca la sincronización. Detecta si estamos en casa y si está operativa la raspberry pi, en cuyo caso hace la llamada a unison. 
- default.prf: definición de los archivos de los que se hace backup, debe colocarse en /root/.unison/. Ahora mismo son iguales tanto en el portátil como en el sobremesa, el del portátil ASUS es distinto porque no puedo llevar al portátil los archivos de datos comunes.
- default-asus.prf: Configuración unison de ASUS, sólo cambia que quita el root donde están los documentos comunes
- unison.service: servicio a copiar como /lib/systemd/system/unison.service
- unison.timer: el temporizador que hace que se ejecute de forma repetitiva
- install.sh: copia los archivos a su sitio. Debe ejecutarse como root (sudo bash install.sh).

## 1. Permitir ejecutar unison como root sin password
Hay que modificar el fichero sudoers, para ello, se edita con `visudo` y se añade al final: 

```
# Permito ejecutar unison como root sin password
%familia ALL=(ALL) NOPASSWD: /usr/bin/unison
```
