#/bin/bash

mkdir -p /root/.unison
ln -s `realpath default.prf` /root/.unison/

mkdir -p /home/bin/
cp run_unison.pl /home/bin/

chown -R david.familia /home/bin

# Permitir unison sin password
cp sudoers.unison /etc/sudoers.d/unison
chown root.root /etc/sudoers.d/unison
chmod 0440 /etc/sudoers.d/unison

for i in david belen clara irene 
do
    mkdir -p /home/$i/.config/plasma-workspace/shutdown
    chown -R $i.familia /home/$i/.config/plasma-workspace/
    ln -s /home/bin/run_unison.pl /home/$i/.config/plasma-workspace/shutdown
done

# Servicio y timer
cp unison.service /lib/systemd/system/
cp unison.timer /lib/systemd/system/

# Activarlos
systemctl enable unison.timer
systemctl start unison.timer
systemctl status unison.timer


echo "En ASUS hay que modificar el enlace simbólico cd /root/.unison/default.prf"
echo "para que apunte a default-asus"
echo "(ln -sf `realpath default-asus.prf` /root/.unison/default.prf)"
