#!/usr/bin/perl

#
# Decide si hay que lanzar o no unison
#

# 1. Si ya se está ejecutando salgo
if(`ps -aef | grep -v grep | grep -v run_unison.pl | grep unison`) {
    print "Process is running!\n";
    exit;
}#if


# Si estoy en la subred de casa y la raspberry está encendida
if (`ip address show | grep "brd 192.168.1.255"`
    && `nmap -sP 192.168.1.4 | grep "Host is up"`) {
    system "sudo /usr/bin/unison";
}

