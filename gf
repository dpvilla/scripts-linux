#!/bin/bash 
#
GLASSFISH_HOME=/usr/glassfish
BACKUPS_DIR=/data/old

#GLASSFISH_HOME=/home/david/programs/payara
#BACKUPS_DIR=/home/david/tmp


##########################################################
#
#  FUNCIONES COPIADAS DE /bin/glassfish pero con modificaciones
#
##########################################################

#
# Calcula la carpeta en la que se encuentra el directorio del payara
# (del servidor) de un dominio concreto, es decir, busca en todos los 
# payaras instalados y mira con cual se está ejecutando el dominio
#
serverDir() {
   local domain=$1
   local count=$(find -L "$GLASSFISH_HOME" -mindepth 3 -name $domain -printf '.' | wc -c)
   if [ $count -le 0 ]
   then
      echoresult FAILED "domain $domain not found"
      exit 1
   else
      if [ $count -gt 1 ]
      then
         echoresult FAILED "multiple domains $domain found"
         exit 1
      fi
   fi
   local serverLocation=$(find -L "$GLASSFISH_HOME" -mindepth 3 -name $domain)
   serverLocation=${serverLocation%/glassfish/domains/$domain}  
   serverDir=$serverLocation
}

echoresult() {
   echo -n " "
   $MOVE_TO_COL
   echo -n "[  "
   case "$1" in
      'OK')
         $SETCOLOR_SUCCESS
      ;;
      'FAILED')
         $SETCOLOR_FAILURE
      ;;
      *)
         $SETCOLOR_WARNING
      ;;
   esac
   echo -n "$1"
   $SETCOLOR_NORMAL
   echo "  ]"
   shift
   if [ "$#" != "0" ] ; then echo "$1" ; fi
}
####################################################################

# Obtiene el asadmin para el dominio especificado
function asadmin_command() {
    if [ ! -z "$asadmin_command" ]; then
        return 0
    fi
    echo "Obteniendo la ruta a asadmin..."
    serverDir $domain
    local server=$serverDir
    local admin_port=62${domain:6}
    local asadmin=$server/bin/asadmin
    asadmin_command="${asadmin} --port $admin_port --passwordfile ${domain_dir}/password"
}


function application_name() {
    asadmin_command
    application_name=$($asadmin_command list-applications | cut -d ' ' -f 1 | head -n1)
    if [ $application_name = "Nothing" ]; then
        echo "No hay aplicaciones instaladas en el dominio $domain"
        return 1
    fi
}

# Obtiene la aplicación instalada en payara y la desinstala
function undeploy() {
    asadmin_command
    application_name
    if [ $? -ne 0 ]; then 
        return $?
    fi
    echo "Undeploying $application_name in $domain"
    $asadmin_command undeploy $application_name
}


function deploy() {
    if [ $# -lt 3 ]; then
        echo "Debe especificar la ruta hacia el ear"
        usage
        exit 2
    fi
    asadmin_command
    local application=$3
    local enabled_param="--enabled=false"
    if [ $# -eq 4 ]; then
        if [ $4 = "enabled" ]; then
            enabled_param="--enabled=true"
        fi
    fi
    echo "Deploying $application in $domain - $enabled_param"
    $asadmin_command deploy $enabled_param $application
    if [ $enabled_param = "--enabled=false" ]; then
        echo "Aplicación desplegada PERO DESACTIVADA. Para activarla puedes hacer:"
        echo "   gf enable $domain"
    fi
}


function enable() {
    asadmin_command
    application_name
    if [ $? -ne 0 ]; then 
        return $?
    fi
    echo "Activando $application_name in $domain"
    $asadmin_command enable $application_name
}

function disable() {
    asadmin_command
    application_name
    if [ $? -ne 0 ]; then 
        return $?
    fi
    echo "Desactivando $application_name in $domain"
    $asadmin_command disable $application_name
}

function backup() {
    local backup_dir=${BACKUPS_DIR}/$(date +%Y%m%d)
    mkdir -p ${backup_dir}
    echo "Creando backup en ${backup_dir}"
    tar -czvf ${backup_dir}/${domain}.tgz --exclude=${domain_dir}/logs ${domain_dir}
}

function redeploy() {
    asadmin_command
    if [ $# -lt 3 ]; then
        echo "Debe especificar la ruta hacia el ear"
        usage
        exit 2
    fi
    undeploy
    echo "****************************************************************"
    echo "* LA APLICACIÓN YA ESTÁ DESACTIVADA, AHORA PUEDES HACER BACKUP *" 
    echo "* Y PASAR LOS SCRIPTS DE ACTUALIZACIÓN DE LA BASE DE DATOS.    *" 
    echo "****************************************************************"

    echo "Restarting $domain"
    $asadmin_command stop-domain $domain
    if [ $? -ne 0 ]; then 
        echo "$domain no está arrancado por lo que no se ha podido hacer undeploy. "
        echo "Arráncalo si quieres actualizar la aplicación "
        exit 1
    fi
    
    echo "Borrando osgi-cache y generated en $domain"
    rm -rf $domain/osgi-cache/* $domain/generated/*
    
    echo "Arrancando $domain"
    glassfish start $domain
    deploy $@
}

function usage() {
    local script_name=$(basename "$0")
    echo "Usage $script_name ACTION DOMAIN OTHER_OPTIONS donde ACTION puede ser"
    echo "  - undeploy: desinstala cualquier aplicación del servidor."
    echo "          $script_name undeploy domain99"
    echo "  - deploy: instala nueva aplicación. "
    echo "          $script_name deploy domain99 /ruta/hacia/aplicacion.ear [enabled]"
    echo "      Si se marca enabled, la aplicación queda activada, sino queda desactivada"
    echo "      (por ejemplo mientras se pasan scripts)"
    echo "  - enable: activa la aplicación que está disabled"
    echo "          $script_name enable domain99"
    echo "  - disable: desactiva la aplicación del servidor (deja de estar disponible)"
    echo "          $script_name enable domain99"
    echo "  - backup: hace un backup sin logs del dominio en $BACKUPS_DIR"
    echo "          $script_name backup domain99"
    echo "  - redeploy: realiza el proceso completo de actualización de una aplicación, "
    echo "      dejando, si no se indica lo contrario, la aplicación desactivada para que "
    echo "      cuando acaben de pasarse los scripts de actualiación de la base de datos,"
    echo "      sólo quede pendiente la activación de la aplicación. "
    echo "          $script_name redeploy domain99 /ruta/hacia/aplicacion.ear [enabled]"
    echo "  - stop, start, restart: llama a los comandos de stop, start y restart de glassfish. "
    echo "  - server: muestra un tail -f del fichero logs/server.log del dominio especificado. "
    echo "  - jvm: muestra un tail -f del fichero logs/jvm.log del dominio especificado. "
    

}

#   alias $NOMBRE-purge-restart="glassfish stop $DOMAIN; rm -rf $DOMAIN_DIR/generated $DOMAIN_DIR/osgi-cache $DOMAIN_DIR/applications/*; glassfish start $DOMAIN; tail -f $DOMAIN_DIR/logs/server.log"


# Verificar número mínimo de parámetro
if [ $# -lt 2 ]; then
    usage
    exit 2
fi

action=$1
domain=$2

# Variables globales
domains_dir=$GLASSFISH_HOME/domains
domain_dir=$domains_dir/$domain

case "$action" in

    'undeploy')
        undeploy
    ;;
    'deploy')
        deploy $@
    ;;
    'enable')
        enable 
    ;;
    'disable')
        disable 
    ;;
    'backup')
        backup 
    ;;
    'redeploy')
        redeploy $@
    ;;
    'stop')
        glassfish stop $domain
    ;;
    'start')
        glassfish start $domain
    ;;
    'restart')
        glassfish restart $domain
    ;;
    'server')
        tail -f ${domain_dir}/logs/server.log
    ;;
    'jvm')
        tail -f ${domain_dir}/logs/jvm.log
    ;;

    *)
        echo "Acción $1 no reconocida."
        usage
        exit 1
    ;;

esac



