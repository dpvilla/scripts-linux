#!/bin/hash

#
# Ejecutar el script en el usuario local mediante bash crear-script-descarga-databases.sh
#

bin_dir=~/bin
script_file=${bin_dir}/dbtolocal

mkdir -p $bin_dir

cat << 'EOF' > $script_file
#!/bin/bash
SOURCE=aplserver@docker01.desarrollo.grupoiris.net:/data/file-server/backup_databases_dir
DEST_DIR=~/Descargas/databases

function usage () {
  echo 'Uso ' $script_name ' accion NOMBRE_BASE_DE_DATOS.                                                '
  echo 'Donde acción puede ser uno de los siguientes valores:                              '
  echo '- descarga (descarga del servidor pero no carga en base de datos)                  '
  echo '- carga (carga en base de datos de una copia local previa)                         '
  echo '- completo (descarga, elimina base de datos anterior, crea base de datos y carga)  '
  echo 'Ejemplo:                                                                           '
  echo '   ' $script_name ' completo Glasfish20Avsis                                                     '
}


function download () {
    echo ""
    echo "$(date): Iniciando descarga desde servidor"
    echo "-----------------------------------------------------------------------"
    mkdir -p ${DEST_DIR}
    # Sincroniza
    local cmd="rsync -rh -P --delete ${SOURCE}/${database} ${DEST_DIR}/"
    echo $cmd
    $cmd
    echo "$(date): Descarga finalizada"
}
 
function load () {
    echo ""
    echo "$(date): Iniciando la carga de ${database} en local"
    echo "-----------------------------------------------------------------------"
    local cmd="createdb ${database}"
    # Crea base de datos 
    echo $cmd
    $cmd
    if [ $? -ne 0 ]
    then
        echo "No es posible crear la base de datos ¿existe?"
        exit 1
    fi    
    cmd="pg_restore -j${threads} -d ${database} ${DEST_DIR}/${database}"
    echo $cmd
    $cmd
    echo "$(date): Carga finalizada"
}



script_name=$(basename "$0")
action=$1
database=$2
#threads=$(nproc)
threads=8

if [ $# -lt 2 ]; then
    usage
    exit 2
fi


case $action in
    descarga) 
        download
        ;;
    carga)
        load
        ;;
    completo)
        dropdb ${database}
        download
        load
        ;;
    *)
        echo "Que no, que está mal..."
        usage
        exit 1
      ;;
esac
EOF

chmod +x ${script_file}
