#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;

system "apt update";
system "apt dist-upgrade";
system "apt autoremove";
system "apt clean";

print "dpkg -C\n";
system "dpkg -C";
print 'Press enter to continue\n';
<>;

print "apt-mark showhold\n";
system "apt-mark showhold";
print 'Press enter to continue\n';
<>;

print "dpkg --audit\n";
system "dpkg --audit";
print 'Press enter to continue\n';
<>;

print "sustituyendo stretch por buster en los sources\n";
system "sed -i.bak 's/stretch/buster/g' /etc/apt/sources.list";
system "sed -i.bak 's/stretch/buster/g' /etc/apt/sources.list.d/*.list";

system "apt update";
system "apt list --upgradable";
print 'Press enter to continue\n';
<>;

system "apt upgrade";
system "apt dist-upgrade";
system "apt install linux-image-amd64";
system "apt autoremove";

system "aptitude search '~c'";
print 'Paquetes eliminados que puedan haber dejado ficheros de configuración en el sistema, intro para eliminar\n';
<>;
system "aptitude purge '~c'";

system "aptitude search '~o'";
print 'Press enter to continue\n';
<>;
system "aptitude purge '~o'";


system "aptitude search '~c'";
print 'Paquetes eliminados que puedan haber dejado ficheros de configuración en el sistema, intro para eliminar\n';
<>;
system "aptitude purge '~c'";

system "aptitude search '~o'";
print 'Paquetes obsoletos, intro para eliminar\n';
<>;
system "aptitude purge '~o'";





