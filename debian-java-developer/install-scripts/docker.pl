#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
#use File::Copy;

my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;

my $daemon_json_file = dirname(__FILE__)."/etc/daemon.json";
my $etc_dir = "/etc";

print "Instalar docker\n";
system "apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common";
my $dockerGPG = q{curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -};
system $dockerGPG;
my $dockerRepoCmd = 'add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian '. $DEBIAN_VERSION . ' stable"';
system $dockerRepoCmd;
system "apt update";
system "apt install docker-ce docker-ce-cli containerd.io docker-compose";


# Mover directorio
system("systemctl stop docker");
system("mv /var/lib/docker /home/docker");
system("ln -s /home/docker /var/lib/docker");
system "cp  $daemon_json_file $etc_dir/";


system("systemctl start docker");

# Instala portanier.io
system "docker volume create portainer_data";
system "docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer";


print "IMPORTANTE\n\nSi el usuario ya existe hay que añadir al usuario al grupo docker. Ejemplo:\n";
print "\tsudo usermod -aG docker david\n";
<>;
