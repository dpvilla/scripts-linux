#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;


# Otras instalaciones que he añadido recientemente
# AdoptOpenJDK (openjdk8)
system "wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -";
system "add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/";
system "apt update";
#system "apt install adoptopenjdk-8-hotspot openjdk-11-jdk";
system "apt install openjdk-11-jdk";

# Alternatives de java para asegurar que está con la versión correcta
system "update-alternatives --config java";
