#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
#use File::Basename;
#use File::Copy;

my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;


system "apt -y install wget";
system "wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -";
system "wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- |  apt-key add -";
system "echo 'deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian bionic contrib' |  tee /etc/apt/sources.list.d/virtualbox.list";
system "apt update";
system "apt install linux-headers-$(uname -r) dkms";
system "apt install virtualbox-6.1";
