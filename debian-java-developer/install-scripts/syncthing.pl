#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;


my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;

print "Syncthing.....\n";

# Add the release PGP keys:
system "curl -s https://syncthing.net/release-key.txt | sudo apt-key add -";

# Add the "stable" channel to your APT sources:
system 'echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list';

# Update and install syncthing:
system "sudo apt update";
system "sudo apt-get install syncthing syncthing-gtk";
