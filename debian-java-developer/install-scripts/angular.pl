#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;

# Visual Studio Code
print "Visual Studio Code\n";
system "curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg";
system "install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/";
system "echo 'deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main' > /etc/apt/sources.list.d/vscode.list";

# Nodejs
system "curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash -";
system "apt-get install -y nodejs gcc g++ make";

# Install Visual y node
system "apt update";
system "apt install code";

# Typescript, ionic
system "npm install -g typescript";
my $angular = q{npm install -g @angular/cli};
system $angular;
system "npm install -g ionic";
