#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;


# Otras instalaciones que he añadido recientemente
# AdoptOpenJDK (openjdk8)
system "wget -O - https://dbeaver.io/debs/dbeaver.gpg.key | apt-key add -";
system "echo 'deb https://dbeaver.io/debs/dbeaver-ce /' > /etc/apt/sources.list.d/dbeaver.list";
system "apt update";
system "apt install dbeaver-ce";
