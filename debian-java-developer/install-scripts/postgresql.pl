#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;


my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;

print "Repositorio de postgresql\n";
system "wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add -";
system "echo 'deb http://apt.postgresql.org/pub/repos/apt/ ${DEBIAN_VERSION}'-pgdg main | tee  /etc/apt/sources.list.d/pgdg.list";
system "apt update";
system "apt install postgresql-12";


#Cambiar ubicación del postgresql
print "Se va  proceder a mover el directorio de postgres a /home/postgresql. Pulsa enter... \n";
<>;
system("service postgresql stop");
# Espero que se pare

sleep(5); 
system("mv /var/lib/postgresql /home/postgresql");
system("ln -s /home/postgresql /var/lib/postgresql");
system("service postgresql start");

