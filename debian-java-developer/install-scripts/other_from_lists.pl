#!/usr/bin/perl
#
# Google chrome
# Skype
# 
#
use strict;
use warnings;
use File::Basename;
use File::Copy;
use Cwd qw(getcwd);


my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;

my $script_sources = dirname(__FILE__)."/sources.list.d";
my $system_sources = "/etc/apt/sources.list.d";

print "Copiando sources.list a /etc/apt/sources.lists.d/\n";

system "cp  $script_sources/* $system_sources/";

# KEYS 
system "curl https://repo.skype.com/data/SKYPE-GPG-KEY | sudo apt-key add -";
system "wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - ";

system "apt update";
system "apt install google-chrome-stable skypeforlinux dbeaver-ce";
