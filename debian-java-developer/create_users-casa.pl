#!/usr/bin/perl

# Creación de usuarios y grupos
system "groupdel familia";
system "groupdel fotos";
system "groupdel musica";
system "groupdel documentos";
system "groupdel david";
system "groupdel belen";
system "groupdel clara";
system "groupdel irene";

system "userdel david";
system "userdel belen";
system "userdel clara";
system "userdel irene";

system "groupadd -f -g 500 familia";
system "groupadd -f -g 501 fotos";
system "groupadd -f -g 502 musica";
system "groupadd -f -g 503 documentos";
system "groupadd -f -g 1502 david";
system "groupadd -f -g 1503 belen";
system "groupadd -f -g 1504 clara";
system "groupadd -f -g 1505 irene";

system "useradd -g familia -m -G adm,cdrom,sudo,dip,plugdev,lpadmin,fotos,musica,documentos,david -u 1502 -s /bin/bash -c \"David Pérez Villanueva\" david";
system "useradd -g familia -m -G adm,cdrom,sudo,dip,plugdev,lpadmin,fotos,musica,documentos,belen -u 1503 -s /bin/bash -c \"Belén Quirós López\" belen";
system "useradd -g familia -m -G cdrom,sudo,dip,plugdev,clara -u 1504 -s /bin/bash -c \"Clara Pérez Quirós\" clara";
system "useradd -g familia -m -G cdrom,sudo,dip,plugdev,irene -u 1505 -s /bin/bash -c \"Irene Pérez Quirós\" irene";

