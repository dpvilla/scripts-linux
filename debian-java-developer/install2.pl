#!/usr/bin/perl
#
# Script de instalación de debian 9
#
use strict;
use warnings;
use File::Basename;
use File::Copy;

#
# CONFIGURATION 
#
my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;



# Posible mejora de fuentes ejecutando como root 
# dpkg-reconfigure fontconfig-config y seleccionando "No" en "¿Activar los
# tipos de letra bitmapped por omisión?

system "install-scripts/postgresql.pl";
system "install-scripts/pgadmin.sh";
system "install-scripts/angular.pl";
system "install-scripts/dbeaver.pl";
system "install-scripts/docker.pl";
system "install-scripts/open-jdk.pl";
system "install-scripts/other_from_lists.pl";
system "install-scripts/netbeans.pl";
system "install-scripts/intellij.pl";
# A 31/03/2021 no funciona el sources
#system "install-scripts/insomnia.sh";
system "install-scripts/dbeaver.pl";
system "install-scripts/slack.pl";
#system "install-scripts/syncthing.pl";

#Hago un último upgrade porque hay actualizaciones al añadir nuevos sources...
system "apt upgrade";

system ("fc-cache -fv");

# Borrado de paquetes que han dejado de ser necesarios
system "sudo apt autoremove";

# Por último el script que activa la limpieza de caché de paquetes
system "../limpiar.sh";
