# Scripts de instalación de debian para desarrollador
Estos scripts están pensados para automatizar la instalación de un equipo de desarrollo de nuestra empresa (java, postgresql, angular, etc).

Para realizar la instalación, se hace la instalación estándar de debian kde separando el /home en una partición distinta y creando un usuario administrador que se utilizará para hacer el resto de configuración. Accedemos con este usuario, descargamos estos ficheros y modificamos el create_users.pl para configurar los usuarios deseados. 

Una vez hecho esto, se ejecuta en este orden:
- sudo -i
- install1.pl

Reiniciar el equipo

- install2.pl
- create_users.pl

**NOTA**: los ficheros *_users_casa.pl son adaptaciones del fichero create_users.pl para configurar los equipos de mi familia.

# SSD
Básicamente el manejo de bloques vacíos en un SSD puede ser de dos formas:

-   Con TRIM continuo
-   Con TRIM periódico

A día de hoy tanto los discos como los controladores de los sistemas de archivos soportan ya correctamente el TRIM contínuo ([https://wiki.archlinux.org/index.php/Solid_state_drive](https://wiki.archlinux.org/index.php/Solid_state_drive)) por lo que en principio es la opción que debemos utilizar

## TRIM CONTINUO

Primero, el disco tiene que soportarlo, hay que ejecutar

	lsblk --discard

Nos saca algo como esto:

```
NAME   DISC-ALN DISC-GRAN DISC-MAX DISC-ZERO
sda           0      512B       2G         0
├─sda1        0      512B       2G         0
├─sda2        0      512B       2G         0
└─sda3        0      512B       2G         0
```

Si las columnas DISC-GRAN o DISC-MAX están a cero, SIGNIFICA QUE EL DISCO NO SOPORTA TRIM CONTINUO

Si las columnas están como aquí, basta con añadir discard a las opciones de montaje de los discos en /etc/fstab:

/dev/sda1  /           ext4  defaults,**discard**   0  1

  

## 1.2 TRIM periódico

 Activar en todas las particiones del SSD la opción noatime (no guardar la hora de último uso, ya no es necesario) y quitar la opción "discard" si está puesta:

- Editar fichero /etc/fstab

```
# / was on /dev/sdb1 during installation
UUID=3ccb26e6-fa81-46ab-a1b6-632f8a127e70 /               ext4    noatime,errors=remount-ro 0       1
# /home was on /dev/sdb2 during installation
UUID=c442ea1f-1194-47f4-a675-d276444099e7 /home           ext4    noatime         0       2
# swap was on /dev/sdb3 during installation
UUID=1777e6f3-a261-41f9-baab-db2c1e258d17 none            swap    sw              0       0
```
-   Activar una tarea semanal que haga la limpieza en el ssd (fstrim).

sudo systemctl enable fstrim.timer
sudo systemctl start fstrim.timer
sudo systemctl status fstrim.timer


