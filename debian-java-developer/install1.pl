#!/usr/bin/perl
#
# Script de instalación de debian
#
use strict;
use warnings;
use File::Basename;
use File::Copy;

#
# CONFIGURATION 
#
my $PACKAGES_NAME = dirname(__FILE__) . "/packages.txt";

my $DEBIAN_VERSION = qx(lsb_release -cs);
chomp $DEBIAN_VERSION;


my $user_response;

#goto START;


# Software necesario para instalación
system "apt install dirmngr apt-transport-https rsync wget curl";


# Borro fichero base.list en sources que no se que hace ahí
system "rm -f /etc/apt/sources.list.d/base.list";

# Para wine
#system "dpkg --add-architecture i386";


# Añadir respositorios
print "Añadiendo repositorios...\n";
print "contrib non-free....\n";

my $sed = q{sed -i 's/main$/main contrib non-free/' /etc/apt/sources.list};
system ("echo", $sed);
system $sed;
system "cat /etc/apt/sources.list";
print "Pulsa enter...\n";
<>;
 

#my $filename = "/etc/apt/sources.list";
#open FILE, ">>$filename" or die "Couldn't open FILE: $filename";


#print "Añadiendo repositorio Virtualbox";
#print FILE "\n# Virtualbox\n";
#print FILE "deb http://download.virtualbox.org/virtualbox/debian ".$DEBIAN_VERSION ." contrib\n";
#print FILE "deb http://download.virtualbox.org/virtualbox/debian stretch contrib\n";
#system "wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -";

#print FILE "\n# WineHQ\n";
#print FILE "deb https://dl.winehq.org/wine-builds/debian/ ".$DEBIAN_VERSION ." main\n";
#system "wget -q https://dl.winehq.org/wine-builds/Release.key -O- | sudo apt-key add -";

#close FILE;



#print "Cambiando máscara por defecto\n";
#system "sed -i.aaa 's/^UMASK[[:blank:]]*[[:digit:]]*/UMASK 002/g' /etc/login.defs";
#system "grep UMASK /etc/login.defs";
#print "Intro para continuar";
#<>;





system("apt-get update");
system("apt-get dist-upgrade");

print "\nAl configurar postfix seleccionar solo correo local, se lee con mutt\n";
print "Pulsa enter...\n";
<>;


# Fichero con la lista de paquetes
open packages_file, "$PACKAGES_NAME" or die "No puedo abrir el fichero con los paquetes a instalar";
my @packages = <packages_file>;
close packages_file;

chomp @packages;
system ("apt-get", "install", @packages);

