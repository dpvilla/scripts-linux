#!/bin/bash
FILE_LIST="/etc/apt/sources.list.d/nodesource.list"

if [ -f "$FILE_LIST" ]; then
    rm -f ${FILE_LIST}.save
    mv ${FILE_LIST} ${FILE_LIST}.save
fi
curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
