#!/bin/bash
EXCLUDES="`dirname "$0"`/rsync-homedir-excludes.txt"
ORIG=${1}
DEST=${2?"$0 ORIGEN DESTINO"}
rsync -aPz --progress --delete --exclude-from=$EXCLUDES $ORIG $DEST 
