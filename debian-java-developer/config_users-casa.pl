#!/usr/bin/perl
#
# Creación de estructura de directorios, propietarios, permisos, etc.
#
use strict;
use warnings;

use File::Copy;

my @USERS= ("david", "belen", "clara", "irene");


# carpetas comunes poner chmod g+s directorio para que los ficheros sean creados por ese grupo
# Propietario de las carpetas comunes
print("Cambiando propietario a las carpetas comunes...\n");
system ("chown -R david.documentos /hdd_data/common_docs/");
`chmod g+s /hdd_data/common_docs/*`;
`chown -R david.fotos /hdd_data/common_docs/fotos*`;
`chown -R david.musica /hdd_data/common_docs/Música`;
system("chown -R david.david '/hdd_data/common_docs/firmas\ digitales'"); 
system("chmod go-rxw '/hdd_data/common_docs/firmas\ digitales'");

print("Carpeta con datos de los que no se hace backup en el ssd\n");
if (not -d "/ssd_data/nobackup") {
    mkdir "/ssd_data/nobackup";
}

foreach my $user (@USERS) 
{
  print("\nConfiguración del usuario $user\n");
  my $home = "/home/$user";
  # Cambiar el propietario del home 
  system("chown -R $user.familia $home");
  # crear directorio temporal de los usuarios
  copy("crear_tmp_arranque/xsessionrc","$home/.xsessionrc") or die "Copy failed: $!";
  system("chown $user.familia $home/.xsessionrc");
  system("chmod +x $home/.xsessionrc");
  
  # Enlaces a documentos comunes
  system("ln -f -s /hdd_data/common_docs/ '$home/Documentos\ compartidos'");
  system("ln -f -s /hdd_data/common_docs/Música $home/Música");
  system("ln -f -s /hdd_data/common_docs/fotos $home/fotos");

  # Directorio propio en el ssd
  my $ssdhome = "/ssd_data/nobackup/$user";
  if (not -d $ssdhome) {
    mkdir $ssdhome;
  }
  if (not -e "$home/ssd") {
    system("ln -s $ssdhome $home/ssd");
  }
  
  if (not -d "$home/ssd/m2") {
    mkdir "$home/ssd/m2";
  }
  if (not -e "$home/.m2") {
    system("ln -s $home/ssd/m2 $home/.m2");
  }
  
  if (not -d "$home/ssd/programs") {
    mkdir "$home/ssd/programs";
  }
  if (not -e "$home/programs") {
    system("ln -s $home/ssd/programs $home/programs");
  }
  
  if (not -d "$home/ssd/trabajo") {
    mkdir "$home/ssd/trabajo";
  }
  if (not -e "$home/trabajo") {
    system("ln -s $home/ssd/trabajo $home/trabajo");
  }  
  
  print("Cambiando propietario a $ssdhome");
  system("chown -R $user.familia $ssdhome");
  
  print("Password para $user\n");
  system("passwd $user");
}
