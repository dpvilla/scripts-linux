#!/usr/bin/perl
use strict;

use File::Copy;


sub create_user {
    my $user = $_[0];
    my $id = $_[1];
    
    system "groupadd -f -g $id $user";
    system "useradd -g aplserver -m -G cdrom,floppy,sudo,audio,dip,video,plugdev,netdev,bluetooth,lpadmin,docker,systemd-journal,$user -u $id -s /bin/bash $user";
    
    # Script que crea el directorio tmp en el home
    copy("crear_tmp_arranque/xsessionrc","$home/.xsessionrc") or die "Copy failed: $!";
    system("chown $user.aplserver $home/.xsessionrc");
    system("chmod +x $home/.xsessionrc");
    
    # Activar usuario en postgresql
    my $sql = "CREATE ROLE $user superuser; ALTER ROLE $user WITH LOGIN; ALTER USER $user WITH PASSWORD '$user';";
    system "echo $sql | sudo -u postgres psql template1";
    
}


# aplserver
system "groupadd -f -g 1500 aplserver";
system "useradd -m -u 1500 -g aplserver -G cdrom,floppy,sudo,audio,dip,video,plugdev,netdev,bluetooth,lpadmin -s /bin/bash aplserver";


########################################################
#
#   OTROS USUARIOS
#
########################################################
#create_user('david', 1502);
#create_user('edu', 1514);
#create_user('natalia', 1525);
#create_user('vmfernandez', 1527);
#create_user('rsobrino', 1539);
#create_user('bdiez', 1541);
#create_user('ereyes', 1546);
#create_user('vretortillo', 1553);
#create_user('vmchaves', 1554);
#create_user('gjurado', 1555);
#create_user('aiglesias', 1557);
#create_user('amval', 1558);
#create_user('apuerta', 1559);
#create_user('smartin', 1560);
#create_user('cgomez', 1561);
#create_user('jlbugallo', 1562);
#create_user('aperez', 1563);
#create_user('isalgado', 1564);
#create_user('inma', 1565);
#create_user('vrepiso', 1566);
#create_user('pclaro', 1567);
#create_user('efernandez', 1568);
create_user('cferrero', 1569);
