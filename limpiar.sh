#!/bin/bash


#
# script para borrar el cache snapd
#
mkdir -p /root/bin

cat << EOF > /root/bin/borrar_cache_snap.sh
#! /bin/bash

if [ -d /var/lib/snapd/cache ]
then 
    /usr/bin/find /var/lib/snapd/cache/ -type f -atime +6 -delete
fi

EOF
chmod +x /root/bin/borrar_cache_snap.sh


#
# Fichero con definición del servicio
#
cat << EOF > /lib/systemd/system/limpiar.service
[Unit]
Description=Limpieza de caches de paquetes

[Service]
Type=oneshot
# Elimina los paquetes de la cache de apt
ExecStart=/usr/bin/apt clean

# Elimina la cache de snapd
ExecStart=/root/bin/borrar_cache_snap.sh

EOF


#
# Fichero con la definición del timer
#
cat << EOF > /lib/systemd/system/limpiar.timer

[Unit]
Description=Limpieza de caches de paquetes una vez a la semana

[Timer]
OnCalendar=weekly
AccuracySec=1h
Persistent=true

[Install]
WantedBy=timers.target

EOF

systemctl enable limpiar.timer
systemctl start limpiar.timer
systemctl status limpiar.timer
