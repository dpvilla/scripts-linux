
# scripts linux
Contiene varios scripts linux utilizados por mí para tareas de administración y similares.
- debian-java-developer: Scripts utilizados para la instalación semiautomatizada de un equipo con debian para 
desarrollo en java.
- crear-script-descarga-databases.sh: Crea automáticamente el script dbtolocal que se encarga de descargar y cargar en local las bases de datos procesadas y reducidas 
- limpiar.sh: Crea un servicio y un timer semanal que limpia paquetes obsoletos
- alias-domains: Crea alias para cada uno de los dominios permitiendo las operaciones habituales
- gf: wrapper de comandos habituales de control de glassfish

## debian-java-developer
Conjunto de scripts utilizados para instalar un equipo estándar de desarrollo java en la empresa. Ver [debian-java-developer/README.md](debian-java-developer/README.md)

## crear-script-descarga-databases.sh
Crea automáticamente el script dbtolocal que se encarga de descargar y cargar en local las bases de datos procesadas y reducidas 

Instalación
:  Ejecutar el script mediante el siguiente comando: 
	`bash crear-script-descarga-databases.sh`

Uso del script
: `dbtolocal  accion NOMBRE_BASE_DE_DATOS.`

Donde acción puede ser uno de los siguientes valores:                              
- descarga (descarga del servidor pero no carga en base de datos)                  
- carga (carga en base de datos de una copia local previa)                         
- completo (descarga, elimina base de datos anterior, crea base de datos y carga)  
Ejemplo:                                                                           
    dbtolocal  completo Glasfish20Avsis                 
    
## limpiar.sh: 
Crea un servicio y un timer semanal que limpia paquetes obsoletos y los activa automáticamente. 
**No es buena idea hacer esto si tienes una conexión lenta a internet**

El timer se encarga de borrar:
- Los paquetes descargados por apt .
- Elimina la cache de snapd

Instalación
: `sudo limpiar.sh`

## alias-domains
Debe ser invocado desde .basrc como . alias-domains o similar. Crea los siguientes alias asociados a los dominios de payara presentes en el sistema:
- domainXX-server (para tail de server.log)
- domainXX-jvm (para tail de jvm.log)
- domainXX-purge-restart (para el servidor, borra osgi-cache y generated y arranca)
- domainXX-backup (hace un tgz del directorio del dominio sin incluir los logs)

## gf
Este comando permite realizar las tareas más habituales de gestión de payara sin necesidad de acceder a la consola de administración.

Está orientado a las labores de actualización de las aplicaciones, permitiendo hacer todo el proceso de actualización con unos pocos comandos que son siempre iguales.

### Comandos disponibles
  - undeploy: desinstala cualquier aplicación del servidor.
          `gf undeploy domain99`
  - deploy: instala nueva aplicación. 
          `gf deploy domain99 /ruta/hacia/aplicacion.ear [enabled]`
      Si se marca *enabled*, la aplicación queda activada, sino queda desactivada
      (por ejemplo mientras se pasan scripts)
  - enable: activa la aplicación que está disabled
          `gf enable domain99`
  - disable: desactiva la aplicación del servidor (deja de estar disponible)
          `gf enable domain99`
  - backup: hace un backup sin logs del dominio en /home/david/tmp
          `gf backup domain99`
  - redeploy: realiza el proceso completo de actualización de una aplicación, 
      dejando, si no se indica lo contrario, la aplicación desactivada para que 
      cuando acaben de pasarse los scripts de actualiación de la base de datos,
      sólo quede pendiente la activación de la aplicación. 
          `gf redeploy domain99 /ruta/hacia/aplicacion.ear [enabled]`
  - stop y start: llama a los comandos de stop y start de blassfish. 

Los comandos deploy y redeploy por defecto dejan la aplicación desactivada ya que lo normal es que aún se estén ejecutando los scripts SQL de backup y actualización y que, cuando estos acaben, se proceda a la activación de la aplicación
